# confluence-jp-docker

# 前提環境
## Dockerの導入
Docker CEを導入したLinuxサーバーを用意します。ここでは、Ubuntu 16.04以上を想定しています。Dockerで実行するConfluenceコンテナは、CentOS7ベースになっています。
Docker CEの導入は、下記の公式のオンラインドキュメントをご覧ください。

Ubuntuの場合 https://docs.docker.com/install/linux/docker-ce/ubuntu/

CentOSの場合 https://docs.docker.com/install/linux/docker-ce/centos/

Docker 導入後に、ご利用中のユーザーでDockerを使えるようにするために、下記を実行します。

$ sudo usermod -aG docker ユーザー名

また、Dockerを自動起動するように設定します。

$ sudo apt-get install -y sysv-rc-conf

$ sudo sysv-rc-conf docker on


## Confluence用データ領域の作成

$ sudo mkdir -p /var/atlassian/application-data/confluence

$ sudo chmod 755 /var/atlassian/application-data/confluence

# Dockerfileのダウンロードとイメージ作成
$ sudo apt-get install -y git

$ git clone https://kolinzlabs@bitbucket.org/kolinzlabs/confluence-jp-docker.git

$ cd confluence-jp-docker

$ docker build -t confluence:690 .


# コンテナの起動
作成したコンテナとは別に別途データベースが必要になります。データベース用サーバーを別途用意し、下記で起動したコンテナから接続できるようにしてください。

PostgreSQLをご使用の場合は、ポート5432で接続とします。
MySQLをご使用の場合は、ポート3306で接続とします。

- データベースに、MySQLを使用した場合

	$ docker run -itd -p 8090:8090 -p 3306:3306 --name confluence-server -v /var/atlassian/application-data/:/var/atlassian/application-data/ confluence:690 /start.sh
	
---------

- データベースに、PostgresSQLを使用した場合

	$ docker run -itd -p 8090:8090 -p 5432:5432 --name confluence-server -v /var/atlassian/application-data/:/var/atlassian/application-data/ confluence:690 /start.sh

# セットアップ画面へのアクセス

Webブラウザで、http://IPアドレスもしくはドメイン名:8090/wiki にアクセスします。英語でセットアップ画面が表示されれば成功です。
画面右上で言語設定を日本語に変更し、セットアップを行います。

# SSLサーバ証明書を使用して、httpsで接続する場合

NginxまたはAapcehをリバースプロキシとして使用する必要があります。

