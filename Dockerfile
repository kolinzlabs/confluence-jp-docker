FROM centos:7

MAINTAINER kolinz <knishikawa@kolinzlabs.com>

# yum update
RUN yum update -y && \
yum install -y epel-release && \
yum upgrade -y && \
yum install -y wget && \
yum clean all

# Set Localtime
RUN ln -sf /usr/share/zoneinfo/Asia/Tokyo /etc/localtime && \
ls -l /etc/localtime

# Create response.varfile for Confluence
RUN touch response.varfile && \
echo launch.application$Boolean=false >> response.varfile && \
echo rmiPort$Long=8000 >> response.varfile && \
echo existingInstallationDir=/opt/Confluence >> response.varfile && \
echo sys.confirmedUpdateInstallationString=false >> response.varfile && \
echo sys.languageId=en >> response.varfile && \
echo sys.installationDir=/opt/atlassian/confluence >> response.varfile && \
echo sys.languageId=en >> response.varfile && \
echo app.confHome=/var/atlassian/application-data/confluence >> response.varfile && \
echo executeLauncherAction$Boolean=true >> response.varfile && \
echo httpPort$Long=8090 >> response.varfile && \
echo portChoice=default >> response.varfile

# Auto Install Confluence 6.9.0
RUN wget https://www.atlassian.com/software/confluence/downloads/binary/atlassian-confluence-6.9.0-x64.bin && \
chmod a+x atlassian-confluence-6.9.0-x64.bin && \
./atlassian-confluence-6.9.0-x64.bin -q -varfile response.varfile && \
rm -f atlassian-confluence-6.9.0-x64.bin

# Install Japanese Fonts
RUN yum install -y unzip nano sed && \
mkdir -p /opt/atlassian/confluence/jre/lib/fonts/fallback && \
yum install -y ibus-kkc ipa-mincho-fonts ipa-gothic-fonts ipa-pmincho-fonts ipa-pgothic-fonts vlgothic-* && \
wget https://oscdl.ipa.go.jp/IPAexfont/IPAexfont00301.zip && \
wget https://oscdl.ipa.go.jp/IPAfont/IPAfont00303.zip && \
unzip IPAexfont00301.zip && \
unzip IPAfont00303.zip && \
cp -a IPAexfont00301/*.ttf /opt/atlassian/confluence/jre/lib/fonts/fallback/ && \
cp -a IPAfont00303/*.ttf /opt/atlassian/confluence/jre/lib/fonts/fallback/ && \
rm -rf IPAexfont00301 && \
rm -rf IPAfont00303 && \
rm -f IPAexfont00301.zip && \
rm -f IPAfont00303.zip

# Change Permission
RUN chown -R confluence:confluence /opt/atlassian/confluence && \
chmod 755 /opt/atlassian/confluence/bin/*.sh

# Change Context path
RUN sed -i 's|<Context path=""|<Context path="/wiki"|' /opt/atlassian/confluence/conf/server.xml

# Create Start Script
RUN touch start.sh && \
echo \#\!/bin/sh >> start.sh && \
echo chown -R confluence:confluence /opt/atlassian/confluence >> start.sh && \
echo chown -R confluence:confluence /var/atlassian/application-data/confluence >> start.sh && \
echo /opt/atlassian/confluence/bin/start-confluence.sh >> start.sh && \
echo /bin/bash >> start.sh
RUN chmod a+x start.sh

# Define working directoryse
WORKDIR /opt/atlassian/confluence

# Define default command
CMD ["start.sh"]